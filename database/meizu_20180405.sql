CREATE TABLE `meizu_evaluate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `goods_id` int(11) unsigned DEFAULT NULL COMMENT '商品id',
  `type` int(11) unsigned DEFAULT NULL COMMENT '1好评 2中评 3差评',
  `star` int(11) DEFAULT NULL COMMENT '星级 1~5',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `content` text COMMENT '评论内容',
  `show_switch` int(11) DEFAULT NULL COMMENT '是否显示',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `deleted_at` int(11) DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品评价表';